def read_line(my_file):
    with open(my_file, 'r') as f:
        line = f.readline()
        while line:
            line.rstrip('\n')
            print line
            line = f.readline()


def read_lines(my_file):
    with open(my_file, 'r') as f:
        line = f.readlines()
        for idx, line_string in enumerate(line):
            edit_line = line_string.rstrip('\n')
            line[idx] = edit_line
        # print line
    return line


def count_men(text_list):
    men_count = 0
    for text_line in text_list:
        if "men " in text_line:
            men_count += 1
    print men_count


if __name__ == "__main__":
    my_file = "/Users/cristinalai/bme590/text_manipulation/example_text.txt"
    my_list = read_lines(my_file)
    count_men(my_list)

